# Gatsby Crie um site PWA com React GraphQL e Netlify CMS

# Ferramentas 
* Gatsby JS
  Plugins : 
   * Gatsby Link API : $ npm install -g gatsby-cli
   * Gatsby Plugin SiteMap : $ npm install --save gatsby-plugin-sitemap   
   * Gatsby Transformer Remark : $ npm install --save gatsby-transformer-remark
   * Gatsby Remark Images / Gatsby Remark Relative Images / Gatsby Remark Lazy Load / LazySizes
     * $ npm install gatsby-remark-images gatsby-remark-relative-images gatsby-remark-lazy-load lazysizes
   * Gatsby Remark Prismjs / PrismJS : $ npm install gatsby-remark-prismjs prismjs
   * React Disqus Comments : $ npm install react-disqus-comments

  Criar o Projeto :
    $ gatsby new <NomeDoProjeto>
  Comandos : 
    * Inicia o servidor e faz hotReload :
    $ gatsby develop
    * Gera os arquivos estáticos 
    $ gatsby build 
    * Server para levantar um servidor encima dos arquivos estáticos gerdos anteriormente
    $ gatsby serve
    Apaga os assets e cache 
    $ gatsby clean


* React

* GraphQl

* Styled Components
  Instalação : 
    $ yarn add styled-components
  Plugin Gatsby-StyledComponents
    $ yarn add gatsby-plugin-styled-components

* Styled Icons
  Instalação : 
    $ yarn add styled-icons
    $ npm install --save styled-icons


* Algolia
* NetlifyCMS
* Netlify






 * Aula 53 de 83 ( Aula Completa e Funcional )