const links = [
  {
    label: "Home",
    url: "/",
  },
  {
    label: "Sobre Mim",
    url: "/about/",
  },
  {
    label: "Portifólio",
    url: "/portifolio/",
  },
]

export default links;