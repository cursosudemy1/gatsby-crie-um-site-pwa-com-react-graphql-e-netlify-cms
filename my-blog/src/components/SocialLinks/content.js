const handle = "johndoe"

const links = [
  {
    label: "Github",
    url: `https://github.com/${handle}`,
  },
  {
    label: "Gitlab",
    url: `https://www.instagram.com/${handle}`,
  },
  {
    label: "Twitter",
    url: `https://twitter.com/${handle}`,
  },
  {
    label: "Youtube",
    url: `https://www.youtube.com/${handle}`,
  },
  {
    label: "Unsplash",
    url: `https://unsplash.com/${handle}`,
  },
]

export default links;