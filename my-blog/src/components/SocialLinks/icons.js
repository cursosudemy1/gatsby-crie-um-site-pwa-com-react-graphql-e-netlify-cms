import { Github } from "styled-icons/boxicons-logos/Github"
import { Twitter } from "styled-icons/boxicons-logos/Twitter"
import { Youtube } from "styled-icons/boxicons-logos/Youtube"
import { Unsplash } from "styled-icons/boxicons-logos/Unsplash"
import { Gitlab } from "styled-icons/boxicons-logos/Gitlab"

const Icons = {
  Github,
  Twitter,
  Youtube,
  Unsplash,
  Gitlab,
}

export default Icons;